// news model
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');


var News = new Schema({
  title: String,
  description: String,
  imageURL:{
    type: String
  }
});

News.plugin(passportLocalMongoose);


module.exports = mongoose.model('news', News);
