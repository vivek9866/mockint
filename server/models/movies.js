// Reviews model
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var Movies = new Schema({
    movieID: String,
    aboutMovie: String,
    Name:String,
    releaseDate: String,
    teaserURL: String,
    trailerrURL: String,
    posterURL: String, 
    cast:{
        hero:String,
        heroIn:String,
        director:String,
        producer:String,
        vilan:String,
        others:String
    },
    images:[],
    videos:[],
    ratings: String,
    updated_at: { type: Date, default: Date.now }
    });

    Movies.plugin(passportLocalMongoose);
    
    
    module.exports = mongoose.model('movieDetails', Movies);