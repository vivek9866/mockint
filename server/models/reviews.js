// Reviews model
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var Reviews = new Schema({
    movieName: String,
    ratings: String,
    review : String,
    story: String,
    positives: String,
    negatives: String,
    technicalReview: String,
    verdict: String,
    updated_at: { type: Date, default: Date.now }
    });

    Reviews.plugin(passportLocalMongoose);
    
    
    module.exports = mongoose.model('reviews', Reviews);